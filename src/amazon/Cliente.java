/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon;

/**
 *
 * @author USUARIO
 */
public class Cliente {
    private String Nombres;
    private String Apellidos;
    private String NombreUsuario;
    private String Contrasenia;
    private String CorreoElectronico;

    public Cliente(String Nombres, String Apellidos, String NombreUsuario, String Contrasenia, String CorreoElectronico) {
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.NombreUsuario = NombreUsuario;
        this.Contrasenia = Contrasenia;
        this.CorreoElectronico = CorreoElectronico;
    }
    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getNombreUsuario() {
        return NombreUsuario;
    }

    public void setNombreUsuario(String NombreUsuario) {
        this.NombreUsuario = NombreUsuario;
    }

    public String getContrasenia() {
        return Contrasenia;
    }

    public void setContrasenia(String Contrasenia) {
        this.Contrasenia = Contrasenia;
    }

    public String getCorreoElectronico() {
        return CorreoElectronico;
    }

    public void setCorreoElectronico(String CorreoElectronico) {
        this.CorreoElectronico = CorreoElectronico;
    } 
}
