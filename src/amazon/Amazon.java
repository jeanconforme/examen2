/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon;

import java.util.Scanner;

/**
 *
 * @author USUARIO
 */
public class Amazon {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese los datos del cliente");
        System.out.println("Nombres - Apellidos - NombreDeUsuario - CorreoElectronico - Contraseña");
        Cliente cliente = new Cliente(s.nextLine(), s.nextLine(), s.nextLine(), s.nextLine(), s.nextLine());
        
        Producto Laptop = new Producto();
        Laptop.setDescripcion("Laptop en Buen estado");
        Laptop.setCantidadenExistencia("Disponible 100 unidades");
        Laptop.setPrecio(250.0);
        
        Producto Camisa = new Producto();
        Camisa.setDescripcion("Camisa marca Polo");
        Camisa.setCantidadenExistencia("Disponible 10 unidades");
        Camisa.setPrecio(25.0);
        
        Producto Pantalon = new Producto();
        Pantalon.setDescripcion("Pantalon ancho color blanco");
        Pantalon.setCantidadenExistencia("Disponible 5 unidades");
        Pantalon.setPrecio(12.5);
        
        CarritoCompra compra1 = new CarritoCompra();
        compra1.setProducto(Laptop);
        
        CarritoCompra compra2 = new CarritoCompra();
        compra2.setProducto(Camisa);
        
        ListaDeProducto listaproducto = new ListaDeProducto();
        listaproducto.AgregarCarritoCompras(compra1);
        listaproducto.AgregarCarritoCompras(compra2);
        
        
        listaproducto.ImprimirListaDeProductos();
}}